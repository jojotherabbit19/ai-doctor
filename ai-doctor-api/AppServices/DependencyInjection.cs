﻿using AppServices.Services;
using Microsoft.Extensions.DependencyInjection;

namespace AppServices
{
    public static class DependencyInjection
    {
        public static IServiceCollection CoreServices(this IServiceCollection services)
        {
            #region Services
            services.AddScoped<MongoDbContext>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IAuthorizeServices, AuthorizeServices>();
            services.AddScoped<IBookingServices, BookingServices>();
            #endregion

            return services;
        }
    }
}
