﻿using AppServices.Entity;
using AppServices.Models;
using AutoMapper;
using MongoDB.Driver;

namespace AppServices.Services
{
    public class BookingServices : IBookingServices
    {
        private readonly MongoDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;

        public BookingServices(MongoDbContext dbContext, IMapper mapper, IClaimsServices claimsServices)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _claimsServices = claimsServices;
        }

        public async Task<ResponseHelper<CreateBookingModel>> CreateBooking(CreateBookingModel model)
        {
            try
            {
                var currentUser = _claimsServices.GetCurrentUser();
                var user = await _dbContext.User.Find(x => x.Id == currentUser).FirstOrDefaultAsync();
                if (currentUser == null || user == null || user.Role != Role.Doctor)
                {
                    return new ResponseHelper<CreateBookingModel> { Errors = "We can't find you in my system." };
                }
                var obj = _mapper.Map<Booking>(model);
                await _dbContext.Booking.InsertOneAsync(obj);
                return new ResponseHelper<CreateBookingModel>
                {
                    Data = model
                };
            }
            catch (Exception ex)
            {
                return new ResponseHelper<CreateBookingModel>
                {
                    Errors = ex.Message
                };
            }
        }

        public async Task<ResponseHelper<List<BookingModel>>> GetAllBooking(string doctorId)
        {
            var data = await _dbContext.Booking.Find(x => x.DoctorId == doctorId).ToListAsync();
            return new ResponseHelper<List<BookingModel>>
            {
                Data = _mapper.Map<List<BookingModel>>(data)
            };
        }

        public async Task<ResponseHelper<UpdateBookingModel>> UpdateBooking(string id, UpdateBookingModel model)
        {
            var now = DateTime.UtcNow.ToLocalTime();
            var obj = await _dbContext.Booking.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (obj == null)
            {
                return new ResponseHelper<UpdateBookingModel> { Errors = $"Can't find bookingId: {id} in my system." };
            }
            var update = Builders<Booking>.Update
                                          .Set(x => x.PatientId, model.PatientId)
                                          .Set(x => x.PaidStatus, model.PaidStatus)
                                          .Set(x => x.Date, model.Date)
                                          .Set(x => x.Price, model.Price)
                                          .Set(x => x.Address, model.Address);

            if (model.BookingStatus == BookingStatus.Cancel)
            {
                if (obj.Date.ToLocalTime() <= now.AddMinutes(30))
                {
                    return new ResponseHelper<UpdateBookingModel> { Errors = $"Can't cancel booking." };
                }
                update = update.Set(x => x.BookingStatus, model.BookingStatus);
            }
            update = update.Set(x => x.BookingStatus, model.BookingStatus);
            await _dbContext.Booking.UpdateOneAsync(x => x.Id == id, update);
            return new ResponseHelper<UpdateBookingModel>
            {
                Data = model
            };
        }
        public async Task<ResponseHelper<RateModel>> Rating(string patientId,
            string doctorId,
            string bookingId,
            string comment,
            decimal rate)
        {
            try
            {
                var rateModel = new RateModel
                {
                    PatientId = patientId,
                    DoctorId = doctorId,
                    BookingId = bookingId,
                    Comment = comment,
                    RateNum = rate
                };
                var obj = _mapper.Map<Rate>(rateModel);
                await _dbContext.Rate.InsertOneAsync(obj);
                var doctor = await _dbContext.User.Find(x => x.Id == doctorId).FirstOrDefaultAsync();
                if (doctor != null)
                {
                    var doctorRates = await _dbContext.Rate.Find(x => x.DoctorId == doctorId).ToListAsync();
                    var numRate = doctorRates.Count;
                    var sumRate = doctorRates.Select(x => x.RateNum).Sum();
                    var update = Builders<User>.Update
                        .Set(x => x.Rate, sumRate / numRate);
                    await _dbContext.User.UpdateOneAsync(x => x.Id == doctorId, update);
                }
                return new ResponseHelper<RateModel>
                {
                    Data = rateModel
                };
            }
            catch (Exception ex)
            {
                return new ResponseHelper<RateModel>
                {
                    Errors = ex.Message
                };
            }
        }

        public async Task<ResponseHelper<BookingDetailsModel>> Booking(string bookingId, BookingRequestModel model)
        {
            var bookingObj = await _dbContext.Booking.Find(x => x.Id == bookingId).FirstOrDefaultAsync();
            if (bookingObj == null)
            {
                return new ResponseHelper<BookingDetailsModel>
                {
                    Errors = $"Not found bookingId:{bookingId}"
                };
            }
            var bookingInfor = new BookingInfor
            {
                PatientName = model.PatientName,
                Note = model.Note,
                PatientPhone = model.PatientPhone
            };
            var update = Builders<Booking>.Update
                                          .Set(x => x.BookingInfor, bookingInfor)
                                          .Set(x => x.PatientId, model.PatientId);
            await _dbContext.Booking.UpdateOneAsync(x => x.Id == bookingId, update);
            return new ResponseHelper<BookingDetailsModel>
            {
                Data = _mapper.Map<BookingDetailsModel>(bookingObj)
            };
        }

        public async Task<ResponseHelper<UpdateBookingModel>> CancelBooking(string id)
        {
            var now = DateTime.UtcNow.ToLocalTime();
            var obj = await _dbContext.Booking.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (obj == null)
            {
                return new ResponseHelper<UpdateBookingModel> { Errors = $"Can't find bookingId: {id} in my system." };
            }

            if (obj.Date.ToLocalTime() < now.AddMinutes(30))
            {
                return new ResponseHelper<UpdateBookingModel> { Errors = $"Can't cancel booking." };
            }
            var update = Builders<Booking>.Update.Set(x => x.BookingStatus, BookingStatus.Cancel);
            await _dbContext.Booking.UpdateOneAsync(x => x.Id == id, update);
            return new ResponseHelper<UpdateBookingModel>
            {
                Data = _mapper.Map<UpdateBookingModel>(obj)
            };
        }

        public async Task<ResponseHelper<BookingDetailsModel>> GetBookingDetail(string id)
        {
            var result = await _dbContext.Booking.Find(x => x.Id == id).FirstOrDefaultAsync();
            return new ResponseHelper<BookingDetailsModel>
            {
                Data = _mapper.Map<BookingDetailsModel>(result)
            };
        }

        public async Task<ResponseHelper<List<BookingDetailsModel>>> GetBookingList(string patientId)
        {
            var result = await _dbContext.Booking.Find(x => x.PatientId == patientId).ToListAsync();
            return new ResponseHelper<List<BookingDetailsModel>>
            {
                Data = _mapper.Map<List<BookingDetailsModel>>(result)
            };
        }
    }
}
