﻿using AppServices.Models;

namespace AppServices.Services
{
    public interface IAuthorizeServices
    {
        Task<ResponseHelper<RegisterUserModel>> CreateUser(RegisterUserModel user);
        Task<ResponseHelper<string>> VerifyUser(LoginModel user);
        Task<ResponseHelper<UserModel>> GetUser();
        Task<ResponseHelper<UserModel>> UpdateUser(string id, RegisterUserModel user);
        Task<ResponseHelper<List<UserModel>?>> GetAllDoctor(int pageIndex = 1, int pageSize = 10);
        Task<ResponseHelper<UserModel>> GetDoctor(string doctorId);
    }
}
