﻿using AppServices.Models;

namespace AppServices.Services
{
    public interface IBookingServices
    {
        Task<ResponseHelper<CreateBookingModel>> CreateBooking(CreateBookingModel model);
        Task<ResponseHelper<UpdateBookingModel>> UpdateBooking(string id, UpdateBookingModel model);
        Task<ResponseHelper<List<BookingModel>>> GetAllBooking(string doctorId);
        Task<ResponseHelper<RateModel>> Rating(string patientId, string doctorId, string bookingId, string comment, decimal rate);
        Task<ResponseHelper<BookingDetailsModel>> Booking(string bookingId, BookingRequestModel model);
        Task<ResponseHelper<UpdateBookingModel>> CancelBooking(string id);
        Task<ResponseHelper<BookingDetailsModel>> GetBookingDetail(string id);
        Task<ResponseHelper<List<BookingDetailsModel>>> GetBookingList(string patientId);
    }
}
