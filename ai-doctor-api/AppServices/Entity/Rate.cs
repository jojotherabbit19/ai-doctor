﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace AppServices.Entity
{
    public class Rate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string? PatientId { get; set; }
        public string? DoctorId { get; set; }
        public string? HospitalId { get; set; }
        public string? BookingId { get; set; }
        public string? Comment { get; set; }
        public decimal RateNum { get; set; }
    }
}
