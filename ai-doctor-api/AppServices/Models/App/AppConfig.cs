﻿namespace AppServices.Models
{
    public class AppConfig
    {
        
    }
    public class MongoDBSettings
    {
        public string ConnectionURI { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
    }
    public class JWTSection
    {
        public string SecretKey { get; set; }
        public int ExpiresInDays { get; set; }
    }
}
