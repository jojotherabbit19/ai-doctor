﻿namespace AppServices
{
    public class ResponseHelper<T>
    {
        public T? Data { get; set; }
        public string? Errors { get; set; }
        public bool HasErrors => Errors?.Length > 0;
    }
}
