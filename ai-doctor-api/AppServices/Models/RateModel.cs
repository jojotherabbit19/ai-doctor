﻿namespace AppServices.Models
{
    public class RateModel
    {
        public string? PatientId { get; set; }
        public string? DoctorId { get; set; }
        public string? BookingId { get; set; }
        public string? Comment { get; set; }
        public decimal RateNum { get; set; }
    }
}
